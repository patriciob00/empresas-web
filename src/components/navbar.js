import React, { useState } from 'react'
import { ImgButton } from '../components/index.js'
import './navbar.scss'

function Navbar({ title = null , goBackFunction = null , hasSearch, searchFunc = null, closeSearch = null, children }) {

 const [searching, setSearching] = useState(false)
 const [searchterm, setSearchTerm] = useState('')

 const fillSearch = (value) => {
  setSearchTerm(value)
  if(searchFunc) searchFunc(value)
 }

 const finishSearch = () => {
  setSearching(false)
  setSearchTerm('')
  closeSearch()
 }
 return (
  <section className="navbar">
   { !searching && 
     <div className={`navbar__default-view ${goBackFunction && title ? '--canBack' : '' }`}>
      { children && children }
      { goBackFunction && title && 
        <div className="back-title">
         <ImgButton icon="leftArrow" onClick={goBackFunction} className="back-btn" />
         <span>{title}</span>
        </div> 
      }
      { hasSearch && 
        <div className="search-btn">
          <ImgButton icon="search" onClick={() => setSearching(true)} />
        </div>
      }
     </div> 
   }
   { searching && 
     <div className="navbar__searching-view">
      <ImgButton icon="search" onClick={() => searchFunc(searchterm)} />
      <input 
       type="text" 
       placeholder="Pesquisar" 
       value={searchterm}
       onChange={({ target: { value }}) => fillSearch(value)}
      />
      <ImgButton icon="close" onClick={finishSearch} />
     </div>
   }
  </section>
 )
}

export default Navbar
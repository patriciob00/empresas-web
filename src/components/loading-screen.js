import React from 'react';
import './loading-screen.scss'

function loading() {
 return (
  <div className="loading-container">
    <div className="spinner" />
  </div>
 )
}

export default loading;
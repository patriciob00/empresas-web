import React from 'react'
import * as Svgs from '../assets/svg/index.js'

import './img-button.scss'

function ImgButton({ onClick, icon, className }) {
 return (
  <button className={className} onClick={onClick}>
   <img src={Svgs[icon]} alt="imagem relativa ao botão"/>
  </button>
 )
}

export default ImgButton
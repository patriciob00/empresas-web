import React from 'react'

import './enterprise-view.scss'

function EnterpriseView({ photo = '', name = '', address, type, onClick }) {
 return (
  <div className='enterprise' onClick={onClick}>
   <div className="enterprise__img">
    <img src={photo} alt="imagem da empresa"/>
   </div>
   <div className="enterprise__data">
    <h4 className="data-name">{ name }</h4>
    <p className="data-type">{ type }</p>
    <small className="data-address">{ address }</small>
   </div>
  </div>
 )
}

export default EnterpriseView
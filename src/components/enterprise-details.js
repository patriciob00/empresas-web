import React, { useState, useEffect } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { Navbar, LoadingScreen } from './index'
import { EnterpriseService } from '../services/index'

import './enterprise-details.scss'

function EnterpriseDetail() {
 const history = useHistory()

 const { id } = useParams()
 
 const [isLoading, setIsLoading] = useState(false)
 const [enterprise, setEnterprise] = useState({ photo: null, name: '', description: ''})

 const loadEnterpriseDetails = async () => {
   setIsLoading(true)
   try {
    const { data: { enterprise = {}} } = await EnterpriseService.getEnterpriseByID(id)
    const obj = { 
     ...enterprise,  
     name: enterprise.enterprise_name,
     photo: `https://empresas.ioasys.com.br/${enterprise.photo}`,
     description: enterprise.description
    }

    setEnterprise(obj)
   } catch (error) {
    console.log(error)
   } finally { setIsLoading(false)}
 }

 useEffect(() => {
  loadEnterpriseDetails()
 }, [])

 return (
  <div className="enterprise-details">
   { isLoading && <LoadingScreen /> }
   <Navbar title={enterprise.name} goBackFunction={() => history.push('/')} />
   <div className='enterprise-details__list-body'>
     <div className="enterprise-view">
       <div className="enterprise-view__img">
        { enterprise.photo && <img src={enterprise.photo} alt="logo da empresa" /> }
       </div>
       <p className="enterprise-view__description">{ enterprise.description }</p>
     </div>
   </div>
  </div>
 )
}

export default EnterpriseDetail
import React from 'react'
import { Navbar, EnterpriseView, LoadingScreen } from './index'
import { useHistory } from 'react-router-dom'

import LogoNav from '../assets/img/logo-nav.png'

import './enterprise-list.scss'

function EnterpriseList({ enterprises = [], toSearch, isLoading = false, closeSearch }) {
 const history = useHistory()

 return (
  <div className="enterprise-list">
   { isLoading && <LoadingScreen /> }
   <Navbar hasSearch searchFunc={toSearch} closeSearch={closeSearch}>
    <img src={LogoNav} alt="Logomarca" className="logo-img" />
   </Navbar>
   <div className={`enterprise-list__list-body ${!enterprises.length ? '--empty-list' : ''}`}>
    { 
     enterprises.map((enterprise, index) => ( 
      <EnterpriseView 
       key={index}
       photo={`https://empresas.ioasys.com.br/${enterprise.photo}`}  
       name={enterprise.enterprise_name}
       description={enterprise.description}
       address={`${enterprise.city} - ${enterprise.country}`}
       type={enterprise.enterprise_type.enterprise_type_name}
       onClick={() => history.push(`/details/${enterprise.id}`)}
      />
     ))
    }

    { !isLoading && !enterprises.length &&
     <h6>Nenhuma Empresa foi encontrada para a busca realizada</h6>
    }
   </div>
  </div>
 )
}

export default EnterpriseList
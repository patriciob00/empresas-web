import LockSvg from './cadeado.svg';
import EmailSvg from './email.svg';
import eyeDisabled from './eyeDisabled.svg';
import Eye from './eye.svg';
import LeftArrow from './left-arrow.svg';
import Search from './search.svg';
import Close from './close.svg';

export { 
 LockSvg as lock, 
 EmailSvg as email,
 eyeDisabled,
 Eye as eye,
 LeftArrow as leftArrow,
 Search as search,
 Close as close,
}
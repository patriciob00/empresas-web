import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { AuthService } from '../services/index.js'
import './login.scss'
import { ImgButton, LoadingScreen } from '../components/index.js'

import LogoImg from '../assets/img/logo-home.png'
import EmailSvg from '../assets/svg/email.svg'
import LockSvg from '../assets/svg/cadeado.svg'

function Login ({ successLogin }) {
 const [loginError, setLoginError] = useState(false)
 const [email, setEmail] = useState('')
 const [password, setPassword] = useState('')
 const [showPassword, setShowPassword] = useState(false)
 const [loading, setLoading] = useState(false)
 const history = useHistory();

 const submitCredentials = async () => {
  try {
   setLoading(true)
   setLoginError(false)
   const { status, headers } = await AuthService.login(email, password)
   if( status === 200 ) {
    await saveHeaders(headers)
    await setLoading(false)
    successLogin()
    history.push('/')
   }
  } catch (error) {
   console.log('error: ', error)
   setLoading(false)
   setLoginError(true)
  }
 }

 const saveHeaders = async (headers) => {
  sessionStorage.clear()
  return await sessionStorage.setItem('headers', JSON.stringify({
   client: headers.client,
   'access-token': headers['access-token'],
   uid: headers.uid 
  }))
 }

 const onKeyDown = ({ key }) => {
  if(key === 'Enter' && email.length && password.length) submitCredentials()
 }

 return (
  <div className="login-container">
   { loading && <LoadingScreen /> }
    <div className="login-container__logo">
      <img src={LogoImg} alt="Logo da ioasys" />
    </div>
    <div className="login-container__welcome">
      <h3>Bem vindos ao <br />empresas</h3>
      <p>Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.</p>
    </div>
    <div className="login-container__form-area">
      <div className={`input-container ${loginError ? '--error' : '' }`}>
        <div className="icon">
         <img src={EmailSvg} alt="imagem de uma carta" />
        </div>
        <input 
         name="email" 
         placeholder="E-mail"
         type="email"
         value={email}
         onChange={({ target: { value }}) => setEmail(value)}
        />
        { loginError && <div className="icon-error">!</div> }
      </div>
      <div className={`input-container ${loginError ? '--error' : '' }`}>
        <div className="icon">
         <img src={LockSvg} alt="imagem de um cadeado"/>
        </div>
        <input 
         name="password"
         placeholder="Senha"
         type={ showPassword ? 'text' : 'password' } 
         value={password} 
         onChange={({ target: { value } })=> setPassword(value)}
         onKeyDown={onKeyDown}
        />
        <ImgButton
         className="input-btn" 
         icon={ showPassword ? 'eyeDisabled' : 'eye'} 
         onClick={() => setShowPassword(!showPassword)} 
        />
        { loginError && <div className="icon-error">!</div> }
      </div>
      
      { loginError && 
        <small className="error-advice">Credenciais informadas são inválidas, tente novamente.</small>
      }
      <div className="submit-btn">
       <button disabled={ !email.length || !password.length } onClick={submitCredentials}>Entrar</button>
      </div>
    </div>
  </div>
 )
}

export default Login
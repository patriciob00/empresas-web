import React, { useState, useEffect } from 'react'
import { Switch, Route } from 'react-router-dom'
import { EnterpriseList, EnterpriseDetails } from '../components/index.js'
import { EnterpriseService } from '../services/index'

function Main() {
 const [enterprises, setEnterprises] = useState([])
 const [loadingEnterprises, setLoadingEnterprises] = useState(false)

 const loadEnterprises = () => {
   setLoadingEnterprises(true);
   return EnterpriseService.getAllEnterprises()
    .then( ({ data }) => setEnterprises(data.enterprises))
    .catch( error => console.log(error))
    .finally(() => {
     setLoadingEnterprises(false)
    })
 }

 const loadEnterprisesWithFilter = (filterTerm) => {
  setLoadingEnterprises(true);
  return EnterpriseService.getEnterpriseWithFilter(filterTerm)
   .then(({ data }) => setEnterprises(data.enterprises))
   .catch( error => console.log(error))
   .finally(() => {
    setLoadingEnterprises(false)
   })
 }

 useEffect(() => {
  loadEnterprises()
 }, [])

 return (
  <Switch>
   <Route path="/details/:id"><EnterpriseDetails /></Route>
   <Route path="/">
    <EnterpriseList 
     load={loadEnterprises} 
     enterprises={enterprises} 
     toSearch={loadEnterprisesWithFilter}
     closeSearch={loadEnterprises} 
     isLoading={loadingEnterprises}
    />
   </Route>
  </Switch>
 )
}

export default Main
import React, { useState, useEffect } from 'react'
import { Switch, Route, Redirect, BrowserRouter as Router } from 'react-router-dom'
import Login from './containers/login.js'
import Main from './containers/main.js'

function App() {
  const [logged, setLogged] = useState(false)

  useEffect(() => {
    const headers = sessionStorage.getItem('headers') || null
    if(!!headers)setLogged(true)
    else setLogged(false)
  }, [])
  
  return (
    <Router>
      <Switch>
        <Route path='/login' render={() => {
          return !logged ? <Login successLogin={() => setLogged(true)} />:
            <Redirect to="/" />
        }} />
        <Route path='/' render={() => { return logged ? <Main /> : <Redirect to='/login' /> }} />
      </Switch>      
    </Router>
  );
}

export default App;

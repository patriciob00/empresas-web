import Client from './config.js'

const headers = JSON.parse(sessionStorage.getItem('headers')) || {}

export const getAllEnterprises = async (params = {}) =>  {
 return await Client.get('/enterprises', { headers, params })
  .then(data => data)
  .catch( error => { throw error })
}

export const getEnterpriseWithFilter = (name = null, enterpriseTypes = null ) => {
let params = {}
 if(enterpriseTypes) params['enterprise_types'] = enterpriseTypes
 if(name) params.name = name

 return getAllEnterprises(params)
}

export const getEnterpriseByID = async (id) => {
 return await Client.get(`/enterprises/${id}`, { headers })
  .then( data => data )
  .catch( error => { throw error })
}
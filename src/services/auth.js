import client from './config.js'

export const login = async (email, password) => {
 return await client.post('/users/auth/sign_in', { email, password })
  .then( data => data )
  .catch( error => { throw error })
}